package no.noroff.sean.trucks;

import no.noroff.sean.cab.BudgetCab;
import no.noroff.sean.cab.Cab;
import no.noroff.sean.wheels.BudgetWheels;
import no.noroff.sean.wheels.Wheels;

public class BudgetTruckFactory implements TruckFactory {
    @Override
    public Wheels createWheels() {
        return new BudgetWheels();
    }

    @Override
    public Cab createCab() {
        return new BudgetCab();
    }
}
