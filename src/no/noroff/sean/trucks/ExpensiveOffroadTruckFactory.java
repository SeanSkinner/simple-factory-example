package no.noroff.sean.trucks;

import no.noroff.sean.cab.Cab;
import no.noroff.sean.cab.ExpensiveCab;
import no.noroff.sean.wheels.ExpensiveOffroadWheels;
import no.noroff.sean.wheels.ExpensiveWheels;
import no.noroff.sean.wheels.Wheels;

public class ExpensiveOffroadTruckFactory implements TruckFactory {
    @Override
    public Wheels createWheels() {
        return new ExpensiveOffroadWheels();
    }

    @Override
    public Cab createCab() {
        return new ExpensiveCab();
    }
}
