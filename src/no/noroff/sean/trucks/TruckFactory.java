package no.noroff.sean.trucks;

import no.noroff.sean.cab.Cab;
import no.noroff.sean.wheels.Wheels;

public interface TruckFactory {
    Wheels createWheels();
    Cab createCab();
}
