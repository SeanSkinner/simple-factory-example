package no.noroff.sean.trucks;

import no.noroff.sean.cab.Cab;
import no.noroff.sean.cab.ExpensiveCab;
import no.noroff.sean.wheels.ExpensiveWheels;
import no.noroff.sean.wheels.Wheels;

public class ExpensiveTruckFactory implements TruckFactory {
    @Override
    public Wheels createWheels() {
        return new ExpensiveWheels();
    }

    @Override
    public Cab createCab() {
        return new ExpensiveCab();
    }
}
