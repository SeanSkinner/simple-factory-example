package no.noroff.sean.trucks;

import no.noroff.sean.cab.Cab;
import no.noroff.sean.wheels.Wheels;

public class Truck {
    private Wheels wheels;
    private Cab cab;

    private TruckFactory truckFactory;

    public Truck(TruckFactory truckFactory) {
        this.truckFactory = truckFactory;
        wheels = truckFactory.createWheels();
        cab = truckFactory.createCab();
    }

    public Wheels getWheels() {
        return wheels;
    }
}
