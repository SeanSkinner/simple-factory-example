package no.noroff.sean;

import no.noroff.sean.trucks.BudgetTruckFactory;
import no.noroff.sean.trucks.ExpensiveOffroadTruckFactory;
import no.noroff.sean.trucks.Truck;

public class Main {

    public static void main(String[] args) {
        var truck = new Truck(new ExpensiveOffroadTruckFactory());
        truck.getWheels().printWheelDetails();

        var truck2 = new Truck(new BudgetTruckFactory());
        truck2.getWheels().printWheelDetails();
    }
}
