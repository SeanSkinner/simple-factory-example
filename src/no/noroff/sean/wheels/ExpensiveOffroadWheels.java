package no.noroff.sean.wheels;

public class ExpensiveOffroadWheels implements Wheels {
    @Override
    public void printWheelDetails() {
        System.out.println("These are FANCY wheels that can take a beating on the road");
    }
}
